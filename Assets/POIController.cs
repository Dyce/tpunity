﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class POIController : MonoBehaviour
{
    // Start is called before the first frame update

    public Shader outlineShader;
    private Shader defaultShader;
    public Renderer renderer;
    public POIDataSO POI;

    void Start()
    {
        defaultShader = renderer.material.shader;
        POI.Discovered = false;
        GameManager.Instance.POIs.Add(POI);
        Debug.Log(GameManager.Instance.POIs.Count);
        Debug.Log(POI.Name);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == "Character")
        {
            renderer.material.shader = outlineShader;
            if(!POI.Discovered)
            {
                POI.Discovered = true;
                EventManager.Instance.POIDiscovered(POI);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        renderer.material.shader = defaultShader;
    }


}
