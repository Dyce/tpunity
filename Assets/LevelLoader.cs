using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class LevelLoader : MonoBehaviour
{
    public Animator transitions;
    public TextMeshProUGUI progressText;
    public Slider progressValue;
    public GameObject loadingScreen;

    public void LoadMainScene()
    {
        loadingScreen.SetActive(true);
        StartCoroutine(loadMainScene());
    }

    IEnumerator loadMainScene()
    {
        yield return null;
        transitions.SetTrigger("Start");
        AsyncOperation loading = SceneManager.LoadSceneAsync("MainScene");
        loading.allowSceneActivation = false;
        while (!loading.isDone)
        {
            int progress = Mathf.RoundToInt((loading.progress * 100));
            progressText.text = progress + " %";
            progressValue.value = progress;

            if (loading.progress >= 0.9f)
            {
                progressText.text = "100 %";
                progressValue.value = 100;
                loading.allowSceneActivation = true;
            }

            yield return null;
        }
        loadingScreen.SetActive(false);
    }
}
