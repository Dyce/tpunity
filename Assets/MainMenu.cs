using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public GameObject mainMenu;
    public GameObject settingsMenu;
    AudioSource source;
    public AudioClip hoverSound;
    public AudioClip clickSound;

    void Start()
    {
        source = GameManager.Instance.GetComponent<AudioSource>();
    }

    public void PlayGame()
    {
        GameManager.Instance.LoadScene(GameManager.Scene.MainScene, GameManager.Scene.MainMenu);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void ShowOptionsMenu()
    {
        LeanTween.delayedCall(0.6f, showOptionsMenu);
    }

    private void showOptionsMenu()
    {
        mainMenu.SetActive(false);
        settingsMenu.SetActive(true);
    }

    public void ShowMainMenu()
    {
        LeanTween.delayedCall(0.6f, showMainMenu);
    }

    private void showMainMenu()
    {
        mainMenu.SetActive(true);
        settingsMenu.SetActive(false);
    }

    public void PlayHoverSound()
    {
        source.PlayOneShot(hoverSound, 2f);
    }

    public void PlayClickSound()
    {
        source.PlayOneShot(clickSound, 2f);
    }
}