using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New POI", menuName = "ScriptableObjects/POI", order = 1)]
public class POIDataSO : ScriptableObject
{
    public string Name;
    public Sprite Image;
    public bool Discovered;
    [TextArea(minLines: 2, maxLines: 5)]
    public string Overview;
    [TextArea(minLines: 2, maxLines: 20)]
    public string Description;
}
