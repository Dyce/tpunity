﻿using System.Text;
using NativeWebSocket;
using UnityEngine;
using Newtonsoft.Json;


public class Networking : MonoBehaviour
{
    public static Networking Instance { get; private set; }
    public static WebSocketState ConnectionState { get { return ws?.State ?? WebSocketState.Closed; } }

    void Awake()
    {
        if(Instance == null)
            Instance = this;
        Connect("ws://wt13.fei.stuba.sk:9000");
    }

    void Update()
    {
#if !UNITY_WEBGL || UNITY_EDITOR
        ws.DispatchMessageQueue();
#endif
    }

    static WebSocket ws;

    public void Connect(string serverURI)
    {
        if(ConnectionState != WebSocketState.Closed)
        {
            Debug.LogWarning("Cannot connect to server (connection already open).");
            return;
        }

        OpenConnection(serverURI);
    }

    public async void SendMessage(byte[] data)
    {
        if(ws.State == WebSocketState.Open)
            await ws.Send(data);
        else
            Debug.LogError($"Cannot send message to server (connection state: {ws.State}");
    }

    public async void SendMessage(string message)
    {
        if(ws.State == WebSocketState.Open)
            await ws.SendText(message);
        else
            Debug.LogError($"Cannot send message to server (connection state: {ws.State}");
    }

    private async void OpenConnection(string serverURI)
    {
        ws = new WebSocket(serverURI);
        ws.OnOpen += Ws_OnOpen;
        ws.OnClose += Ws_OnClose;
        ws.OnMessage += Ws_OnMessage;
        ws.OnError += Ws_OnError;

        EventManager.Instance.ServerConnectionStatusChanged(WebSocketState.Connecting);
        await ws.Connect();
    }

    private void Ws_OnError(string errorMsg)
    {
        Debug.LogError($"Networking error: {errorMsg}");
        EventManager.Instance.ServerConnectionStatusChanged(ws.State);
    }

    private void Ws_OnMessage(byte[] data)
    {
        string message = Encoding.UTF8.GetString(data);
        ServerMessage  msg = JsonConvert.DeserializeObject<ServerMessage>(message);
        EventManager.Instance.InputReceived(msg);
    }

    private void Ws_OnClose(WebSocketCloseCode closeCode)
    {
        Debug.Log($"Server connection closed. (Code: {closeCode})");
        EventManager.Instance.ServerConnectionStatusChanged(ws.State);
    }

    private void Ws_OnOpen()
    {
        Debug.Log($"Established server connection.");
        EventManager.Instance.ServerConnectionStatusChanged(ws.State);
        SendMessage($"{{\"deviceID\":\"{GameManager.Preferences.DeviceName}\"}}");
    }
}
