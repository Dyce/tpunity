﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{   
    public CharacterController controller;
    public Transform playerBody;
    public float movementSpeed = 10f;
    public float rotationSpeed = 100f;
    public float gravity = -9.81f;
    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;

    Vector3 velocity;
    bool isGrounded;
    Vector3 lastPosition;
    float distanceTraveled;

    private void Start()
    {
        lastPosition = transform.position;
    }

    private void CalculateDistance()
    {
        if(!Vector3.Equals(lastPosition, transform.position))
        {
            distanceTraveled += Vector3.Distance(lastPosition, transform.position);
            lastPosition = transform.position;
            EventManager.Instance.DistanceTravelledChanged(distanceTraveled);
        }
    }

    // Update is called once per frame
    void Update()
    {
        CalculateDistance();
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        if(isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }

        //float x = Input.GetAxis("Horizontal");
        //float z = Input.GetAxis("Vertical");

        float x = InputManager.Rotation;
        float z = InputManager.Movement;

        //float strafe = Input.GetAxis("Strafe");

        Vector3 motion = transform.forward * z;
        //Vector3 strafeMotion = transform.right * strafe;

        controller.Move(motion * movementSpeed * Time.deltaTime);
        //controller.Move(strafeMotion * movementSpeed * Time.deltaTime);
        playerBody.Rotate(Vector3.up * x * rotationSpeed * Time.deltaTime);

        velocity.y += gravity * Time.deltaTime;

        controller.Move(velocity * Time.deltaTime);
    }
}

public static class InputManager
{
    public static float Movement { get; set; }
    public static float Rotation { get; set; }

    static InputManager()
    {
        Movement = 0f;
        Rotation = 0f;
        Debug.Log("InputManager");
        EventManager.Instance.onInputReceived += onInputReceived;
    }

    private static void onInputReceived(ServerMessage message)
    {
        Debug.Log("Input received");
        Movement = message?.Speed / 36f ?? Movement;
        Rotation = message?.Direction ?? Rotation;
        Debug.Log($"Movement: {Movement}/{message.Speed}, Rotation: {Rotation}/{message.Direction}");
    }
}
