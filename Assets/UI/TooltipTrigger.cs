﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TooltipTrigger : MonoBehaviour
{
    public string Header;
    public string Content;
    public Vector2 Offset;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Character")
        {
            TooltipSystem.Show(Header, Content, gameObject.transform, Offset);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == "Character")
        {
            TooltipSystem.Hide();
        }
    }
}
