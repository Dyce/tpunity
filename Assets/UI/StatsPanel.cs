using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class StatsPanel : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        ConnectionImage.color = new Color(0.7f, 0.2f, 0.2f);
        onServerConnectionStatusChanged(Networking.ConnectionState);
        SpeedText.text = "0 km/h";

        EventManager.Instance.onServerConnectionStatusChanged += onServerConnectionStatusChanged;
        EventManager.Instance.onPOIDiscovered += onPOIDiscovered;
        EventManager.Instance.onDistanceTravelledChanged += onDistanceTravelledChanged;
        EventManager.Instance.onCollectibleCollected += onCollectibleCollected;
        EventManager.Instance.onInputReceived += onInputReceived;

        LeanTween.delayedCall(1f, OnStart);
    }

    private void OnStart()
    {
        _POICount = GameManager.Instance.POIs.Count;
        _POIDiscovered = 0;
        POIText.text = $"{_POIDiscovered} / {_POICount}";
        DistanceText.text = "0 m";
        _starsCount = GameManager.Instance.CollectiblesCount;
        StarsCollectedText.text = $"{_starsCollected} / {_starsCount}";
    }

    private void onInputReceived(ServerMessage message)
    {
        if(message.Speed == null)
            return;
        SpeedText.text = $"{Mathf.RoundToInt((float)message.Speed)} km/h";
    }

    private void onCollectibleCollected()
    {
        _starsCollected++;
        StarsCollectedText.text = $"{_starsCollected} / {_starsCount}";
    }

    private void onDistanceTravelledChanged(float distance)
    {
        if(distance > 1000)
            DistanceText.text = $"{Math.Round(distance/1000, 1)} km";
        else
            DistanceText.text = $"{Mathf.RoundToInt(distance)} m";
    }

    public Image ConnectionImage;
    public TextMeshProUGUI ConnectionText;
    public TextMeshProUGUI POIText;
    public TextMeshProUGUI DistanceText;
    public TextMeshProUGUI StarsCollectedText;
    public TextMeshProUGUI SpeedText;

    private int _POICount;
    private int _POIDiscovered;
    private int _starsCount;
    private int _starsCollected = 0;

    private void onPOIDiscovered(POIDataSO POI)
    {
        _POIDiscovered++;
        POIText.text = $"{_POIDiscovered} / {_POICount}";
    }

    private void onServerConnectionStatusChanged(NativeWebSocket.WebSocketState state)
    {
        switch(state)
        {
            case NativeWebSocket.WebSocketState.Connecting:
                ConnectionImage.color = new Color(0.8f, 0.5f, 0f);
                ConnectionText.text = "Connecting";
                break;
            case NativeWebSocket.WebSocketState.Open:
                ConnectionImage.color = new Color(0f, 0.6f, 0f);
                ConnectionText.text = "Connected";
                break;
            case NativeWebSocket.WebSocketState.Closed:
                ConnectionImage.color = new Color(0.7f, 0.2f, 0.2f);
                ConnectionText.text = "Disconnected";
                break;
            default:
                Debug.Log($"Connection state: {state}");
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
