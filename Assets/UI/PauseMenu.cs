﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour
{
    public Material BackgroundMaterial;
    public float TransitionDuration = 1f;

    public AudioClip hoverSound;
    public AudioClip clickSound;
    AudioSource source;
    //public GameObject DialogWindow;

    void OnEnable()
    {
        source = GameManager.Instance.GetComponent<AudioSource>();
        LeanTween.value(0f, 0.004f, TransitionDuration)
            .setOnUpdate((float value) =>
            {
                BackgroundMaterial.SetFloat("_BlurAmount", value);
            })
            .setIgnoreTimeScale(true);


        LeanTween.value(1f, 0.5f, TransitionDuration)
            .setOnUpdate((float value) =>
            {
                BackgroundMaterial.SetColor("_Color", new Color(value, value, value, 1f));
            })
            .setIgnoreTimeScale(true);
    }

    private void OnDisable()
    {
        BackgroundMaterial.SetFloat("_BlurAmount", 0f);
        BackgroundMaterial.SetColor("_Color", new Color(1f, 1f, 1f, 1f));
    }

    public void ResumeGameButton()
    {
        LeanTween.delayedCall(0.6f, resumeGameButton);
    }

    private void resumeGameButton()
    {
        UserInterface.Instance.ClosePauseMenu();
    }

    public void PlayHoverSound()
    {
        source.PlayOneShot(hoverSound, 2f);
    }

    public void PlayClickSound()
    {
        source.PlayOneShot(clickSound, 2f);
    }

    public void QuitButton()
    {
        LeanTween.delayedCall(0.6f, quitButton);
    }

    private void quitButton()
    {
        UserInterface.Instance.QuitGame();
    }

    public void OptionsButton()
    {

    }

    public void LeaderboardsButton()
    {

    }
}
