using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[ExecuteInEditMode()]
public class Tooltip : MonoBehaviour
{
    public TextMeshProUGUI Header;
    public TextMeshProUGUI Content;
    public LayoutElement LayoutElement;
    public int CharacterWrapLimit;

    public void SetText(string header, string content)
    {
        if (string.IsNullOrEmpty(header))
            Header.gameObject.SetActive(false);
        else
        {
            Header.gameObject.SetActive(true);
            Header.text = header;
        }

        if (string.IsNullOrEmpty(content))
            Content.gameObject.SetActive(false);
        else
        {
            Content.gameObject.SetActive(true);
            Content.text = content;
        }

        int headerLength = Header.text.Length;
        int contentLength = Content.text.Length;

        LayoutElement.enabled = (headerLength > CharacterWrapLimit || contentLength > CharacterWrapLimit);
    }
}
