﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using System;

public class POIDialogAnimation : MonoBehaviour
{
    public Material BackgroundMaterial;
    public float TransitionDuration = 1f;
    //public GameObject DialogWindow;

    void OnEnable()
    {
        LeanTween.value(0f, 0.004f, TransitionDuration)
            .setOnUpdate((float value) =>
            {
                BackgroundMaterial.SetFloat("_BlurAmount", value);
            })
            .setIgnoreTimeScale(true);


        LeanTween.value(1f, 0.5f, TransitionDuration)
            .setOnUpdate((float value) =>
            {
                BackgroundMaterial.SetColor("_Color", new Color(value, value, value, 1f));
            })
            .setIgnoreTimeScale(true);
            //.setOnComplete(() => 
            //{
            //    DialogWindow.SetActive(true);
            //});
    }
}
