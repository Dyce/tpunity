using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using System;

public class GameManager : MonoBehaviour
{

    public static GameManager Instance { get; private set; }
    public Animator transitions;
    public TextMeshProUGUI progressText;
    public Slider progressValue;
    public GameObject loadingScreen;
    public AudioSource MusicSource;


    public List<POIDataSO> POIs = new List<POIDataSO>();
    public int CollectiblesCount;

    List<AsyncOperation> operations;
    float totalProgress;
    public void LoadScene(Scene scene, Scene? currentScene)
    {
        loadingScreen.SetActive(true);
        StartCoroutine(LoadSceneAsync(scene, currentScene));
    }

    //IEnumerator LoadSceneAsync(Scene scene, Scene? currentScene)
    //{
    //    yield return new WaitForSeconds(5);
    //    Debug.Log("LoadingScreen");
    //    List<AsyncOperation> operations = new List<AsyncOperation>();

    //    operations.Add(SceneManager.UnloadSceneAsync((int)currentScene));
    //    operations.Add(SceneManager.LoadSceneAsync((int)scene, LoadSceneMode.Additive));
    //    operations[1].allowSceneActivation = false;
    //    Debug.Log("Added operations");
    //    float totalProgress;

    //    for (int i = 0; i < operations.Count; i++)
    //    {
    //        Debug.Log("Operation: " + i);
    //        while(operations[i].progress < 0.9f)
    //        {
    //            totalProgress = 0f;

    //            foreach (AsyncOperation operation in operations)
    //            {
    //                totalProgress += (operation.progress / .9f);
    //            }

    //            totalProgress = Mathf.RoundToInt((totalProgress / operations.Count) * 100);
    //            Debug.Log("Total progress: " + totalProgress);
    //            progressText.text = totalProgress + " %";
    //            progressValue.value = totalProgress;
    //            yield return null;
    //        }
    //    }

    //    Debug.Log("Activate scene");
    //    operations[1].allowSceneActivation = true;
    //    yield return null;
    //    Debug.Log("Activated");
    //    transitions.SetTrigger("End");
    //    yield return new WaitForSeconds(2);
    //    loadingScreen.SetActive(false);
    //    Debug.Log("Done");
    //}

    IEnumerator LoadSceneAsync(Scene scene, Scene? currentScene)
    {
        yield return new WaitForSeconds(1);
        operations = new List<AsyncOperation>();
        if(currentScene != null)
            operations.Add(SceneManager.UnloadSceneAsync((int)currentScene));
        operations.Add(SceneManager.LoadSceneAsync((int)scene, LoadSceneMode.Additive));
        StartCoroutine(GetLoadProgress(scene));

    }

    public IEnumerator GetLoadProgress(Scene scene)
    { 
        for (int i = 0; i < operations.Count; i++)
        {
            while (!operations[i].isDone)
            {
                totalProgress = 0.1f;

                foreach (AsyncOperation operation in operations)
                {
                    totalProgress += operation.progress;
                }
                 
                totalProgress = Mathf.RoundToInt((totalProgress / operations.Count) * 100f);
                progressText.text = totalProgress + " %";
                progressValue.value = totalProgress;
                yield return null;
            }
        }
        var collection = Resources.Load<ShaderVariantCollection>("NewShaderVariants");

        if(collection != null)
        {
            Debug.Log("Shaders/variants: " + collection.shaderCount + "/" + collection.variantCount);

            collection.WarmUp();

            Resources.UnloadAsset(collection);
        }
        yield return new WaitForSeconds(1);
        SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex((int)Scene.MainScene));
        transitions.SetTrigger("End");
        yield return new WaitForSeconds(2);
        loadingScreen.SetActive(false);
    }

    public static class Preferences
    {
        public static string DeviceName { get; set; }
        public static string NickName { get; set; }
        public static float MasterVolume { get; set; }
        public static float MusicVolume { get; set; }

        static Preferences()
        {
            DeviceName = PlayerPrefs.GetString("DeviceName", "undefined");
            NickName = PlayerPrefs.GetString("NickName", "Player");
            MasterVolume = PlayerPrefs.GetFloat("MasterVolume", 1f);
            MusicVolume = PlayerPrefs.GetFloat("MusicVolume", 1f);
        }

        public static void Save()
        {
            PlayerPrefs.SetString("DeviceName", DeviceName);
            PlayerPrefs.SetString("NickName", NickName);
            PlayerPrefs.SetFloat("MasterVolume", MasterVolume);
            PlayerPrefs.SetFloat("MusicVolume", MusicVolume);
            PlayerPrefs.Save();
        }
    }

    public enum Scene
    {
        GameManager = 0,
        MainMenu = 1,
        MainScene = 2
    }

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
           //DontDestroyOnLoad(gameObject);
        }
        AudioListener.volume = Preferences.MasterVolume;
        MusicSource.volume = Preferences.MusicVolume;
        SceneManager.LoadSceneAsync((int)Scene.MainMenu, LoadSceneMode.Additive);
    }
}
