﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UserInterface : MonoBehaviour
{
    public static UserInterface Instance { get; private set; }

#pragma warning disable IDE0051 // Remove unused private members
    void Awake()
#pragma warning restore IDE0051 // Remove unused private members
    {
        if (Instance == null)
            Instance = this;
    }

    public Image image;
    public TextMeshProUGUI overview;
    public TextMeshProUGUI description;
    public GameObject POIDialog;
    [Space(20)]
    public GameObject PauseMenu;
    public KeyCode PauseKey;
    [Space(20)]
    public GameObject HUD;

    void Update()
    {
        if(Input.GetKeyDown(PauseKey))
            if(PauseMenu.activeInHierarchy)
            {
                ClosePauseMenu();
                HUD.SetActive(true);
            }
            else
            {
                HUD.SetActive(false);
                ShowPauseMenu();
            }
    }

    public void ShowPOIDialog(POIDataSO POIData)
    {
        overview.text = POIData.Overview;
        description.text = POIData.Description;
        image.sprite = POIData.Image;
        HUD.SetActive(false);
        POIDialog.SetActive(true);
        Time.timeScale = 0;
    }

    public void ClosePOIDialog()
    {
        POIDialog.SetActive(false);
        HUD.SetActive(true);
        Time.timeScale = 1;
    }

    public void ShowPauseMenu()
    {
        PauseMenu.SetActive(true);
        Time.timeScale = 0;
    }

    public void ClosePauseMenu()
    {
        PauseMenu.SetActive(false);
        Time.timeScale = 1;
    }
    
    public void QuitGame()
    {
        Application.Quit();
    }
}
