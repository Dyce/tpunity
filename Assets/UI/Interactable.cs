﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using System;

[Serializable]
//public class POIDialogEvent : UnityEvent<POIDialogInfo> { }

public class Interactable : MonoBehaviour
{
    public KeyCode interactionKey;
    //public EventTrigger.TriggerEvent customCallback;
    public UnityEvent OnInteraction;


    void Update()
    {
        if (Input.GetKeyDown(interactionKey))
        {
            OnInteraction.Invoke();
        }
    }
}
