﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TooltipInteraction : MonoBehaviour
{
    public GameObject tooltip;
    private bool isDisplayed = false;
    private new Transform camera;
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Character")
        {
            camera = Camera.main.transform;
            tooltip.SetActive(true);
            isDisplayed = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == "Character")
        {
            tooltip.SetActive(false);
            isDisplayed = false;
        }
    }

#pragma warning disable IDE0051 // Remove unused private members
    private void Update()
#pragma warning restore IDE0051 // Remove unused private members
    {
        if (isDisplayed)
        {
            Vector3 screen = new Vector3(camera.position.x, tooltip.transform.position.y, camera.position.z);
            tooltip.transform.LookAt(screen);
        }
    }
}
