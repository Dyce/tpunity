﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TooltipSystem : MonoBehaviour
{
    private static TooltipSystem Instance;
    private static bool isDisplayed;
    private static Transform target;
    private static Vector2 Offset;

    public void Awake()
    {
        Instance = this;
        isDisplayed = false;
    }


    public Tooltip tooltip;

    public static void Show(string header, string content, Transform targetTransform, Vector2 offset)
    {
        if(isDisplayed)
        {
            Debug.LogError("Tooltip is already displayed!");
            return;
        }
        isDisplayed = true;
        target = targetTransform;
        Offset = offset;
        Instance.tooltip.SetText(header, content);
        Instance.tooltip.gameObject.SetActive(true);
    }

    public static void Hide()
    {
        isDisplayed = false;
        Instance.tooltip.gameObject.SetActive(false);
    }

    private void Update()
    {

    }

}
