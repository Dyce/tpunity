﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Assets.UI
{
    class POIDialogNavigation : MonoBehaviour
    {
        public KeyCode CloseKey;
        public KeyCode PreviousPageKey;
        public KeyCode NextPageKey;
        public TextMeshProUGUI DescriptionText;
        public Button NextPageButton;
        public Button PreviousPageButton;

        private int DescriptionPageCount;


        void Start()
        {
            DescriptionText.ForceMeshUpdate();
            OnPropertyChanged();
        }

        void Update()
        {
            //if (DescriptionText.havePropertiesChanged)
            //    OnPropertyChanged();

            if (Input.GetKeyDown(CloseKey))
                CloseButtonClick();

            if (Input.GetKeyDown(PreviousPageKey))
                PreviousPageClick();

            if (Input.GetKeyDown(NextPageKey))
                NextPageClick();
        }

        void OnPropertyChanged()
        {
            DescriptionPageCount = DescriptionText.textInfo.pageCount;
            PreviousPageButton.interactable = false;
            
            if (DescriptionText.textInfo.pageCount == 1)
                NextPageButton.interactable = false;
            else
                NextPageButton.interactable = true;


            DescriptionText.pageToDisplay = 1;
            
            Debug.Log("Pages: " + DescriptionText.textInfo.pageCount);
        }

        public void CloseButtonClick()
        {
            Debug.Log("Close button clicked.");
            UserInterface.Instance.ClosePOIDialog();
        }

        public void PreviousPageClick()
        {
            if (!PreviousPageButton.interactable)
                return;
            Debug.Log("Previous page button clicked.");
            //DescriptionText.pageToDisplay = (DescriptionText.pageToDisplay - 1) > 0 ? (DescriptionText.pageToDisplay - 1) : DescriptionPageCount;
            DescriptionText.pageToDisplay--;
            if (DescriptionText.pageToDisplay == 1)
                PreviousPageButton.interactable = false;
            if (DescriptionText.pageToDisplay == DescriptionPageCount -1)
                NextPageButton.interactable = true;
        }

        public void NextPageClick()
        {
            if (!NextPageButton.interactable)
                return;
            Debug.Log("Next page button clicked.");
            DescriptionText.pageToDisplay++;
            if (DescriptionText.pageToDisplay == DescriptionPageCount)
                NextPageButton.interactable = false;
            if (DescriptionText.pageToDisplay == 2)
                PreviousPageButton.interactable = true;

        }
    }
}
