﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class SettingsMenu : MonoBehaviour
{
    void OnEnable()
    {
        DeviceName.text = GameManager.Preferences.DeviceName;
        Debug.LogWarning(DeviceName.text);
        NickName.text = GameManager.Preferences.NickName;
        MasterVolume.value = GameManager.Preferences.MasterVolume*100;
        MusicVolume.value = GameManager.Preferences.MusicVolume*100;
        MasterVolumeText.text = (GameManager.Preferences.MasterVolume * 100).ToString();
        MusicVolumeText.text = (GameManager.Preferences.MusicVolume * 100).ToString();
    }

    public TMP_InputField DeviceName;
    public TMP_InputField NickName;
    public Slider MasterVolume;
    public Slider MusicVolume;
    public TextMeshProUGUI MasterVolumeText;
    public TextMeshProUGUI MusicVolumeText;


    public void OnMasterVolumeChanged(float value)
    {
        AudioListener.volume = value/100;
        GameManager.Preferences.MasterVolume = value/100;
        MasterVolumeText.text = value.ToString();
    }

    public void OnMusicVolumeChanged(float value)
    {
        GameManager.Instance.MusicSource.volume = value/100;
        GameManager.Preferences.MusicVolume = value/100;
        MusicVolumeText.text = value.ToString();
    }

    public void SaveSettings()
    {
        GameManager.Preferences.DeviceName = DeviceName.text;
        GameManager.Preferences.NickName = NickName.text;
        GameManager.Preferences.Save();
    }
}

