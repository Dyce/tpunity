﻿using System;
using NativeWebSocket;

public class EventManager
{
    public static EventManager Instance { get; } = new EventManager();


    public event Action<WebSocketState> onServerConnectionStatusChanged;
    public void ServerConnectionStatusChanged(WebSocketState webSocketState)
    {
        onServerConnectionStatusChanged?.Invoke(webSocketState);
    }

    public event Action<POIDataSO> onPOIDiscovered;
    public void POIDiscovered(POIDataSO POI)
    {
        onPOIDiscovered?.Invoke(POI);
    }

    public event Action<float> onDistanceTravelledChanged;
    public void DistanceTravelledChanged(float newDistance)
    {
        onDistanceTravelledChanged?.Invoke(newDistance);
    }

    public event Action onCollectibleCollected;
    public void CollectibleCollected()
    {
        onCollectibleCollected?.Invoke();
    }

    public event Action<ServerMessage> onInputReceived;
    public void InputReceived(ServerMessage msg)
    {
        onInputReceived?.Invoke(msg);
    }
}
