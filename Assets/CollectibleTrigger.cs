﻿using System.Linq;
using UnityEngine;

public class CollectibleTrigger : MonoBehaviour
{
    public AudioSource SoundToPlay;
    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == "Character")
        {
            EventManager.Instance.CollectibleCollected();
            SoundToPlay.Play();
            gameObject.GetComponent<Renderer>().enabled = false;
            Destroy(gameObject, 2f);
        }
    }
}