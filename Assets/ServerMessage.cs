﻿using System;

[Serializable]
public class ServerMessage
{
    public float? Speed;
    public float? Direction;
    public string Arrow;
    public int? Button;
}