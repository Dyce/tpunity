﻿using System.Linq;
using UnityEngine;

public class Collectibles : MonoBehaviour
{
    public GameObject Collectible;
    public int SpawnCount;
    public Transform[] SpawnPoints;

    void Start()
    {
        Transform[] spawPositions = SpawnPoints.OrderBy(x => Random.value).Take(SpawnCount).ToArray();
        foreach(var position in spawPositions)
        {
            Instantiate(Collectible, position);
        }
        GameManager.Instance.CollectiblesCount = spawPositions.Length;
    }
}