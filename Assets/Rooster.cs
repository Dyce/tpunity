﻿using UnityEngine;

public class Rooster : MonoBehaviour
{
    public AudioClip sound;
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == "Character")
        {
            GameManager.Instance.GetComponent<AudioSource>().PlayOneShot(sound, 0.4f);
        }
    }

}