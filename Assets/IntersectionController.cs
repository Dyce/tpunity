﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class IntersectionController : MonoBehaviour
{
    public TrafficLightGroup[] trafficLightGroups;
    void Start()
    {
        StartCoroutine(StartTrafficLights());
    }

    public IEnumerator StartTrafficLights()
    {
        List<Coroutine> coroutines = new List<Coroutine>();
        while(true)
        {
            foreach(var group in trafficLightGroups)
            {
                foreach(var trafficLight in group.trafficLights)
                {
                    coroutines.Add(StartCoroutine(trafficLight.DoNormalCycle(group.GreenTime)));
                }
                foreach(var coroutine in coroutines)
                {
                    yield return coroutine;
                }
                coroutines.Clear();
            }
        }
    }


    [Serializable]
    public class TrafficLightGroup
    {
        public TrafficLightController[] trafficLights;
        public int GreenTime;
    }
}
