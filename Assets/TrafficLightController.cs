﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrafficLightController : MonoBehaviour
{
    public TrafficLightState CurrentState { get; private set; }

    public Renderer LightsRenderer;

    bool yellowLightEnabled;
    void Start()
    {
        LightsRenderer.materials[(int)MaterialColor.Red].SetColor("_EmissionColor", Color.red);
        LightsRenderer.materials[(int)MaterialColor.Yellow].SetColor("_EmissionColor", Color.yellow);
        LightsRenderer.materials[(int)MaterialColor.Green].SetColor("_EmissionColor", Color.green);
        LoadRedState();
    }

    public IEnumerator DoNormalCycle(int greenTime)
    {
        yield return null;
        LoadRedYellowState();
        yield return new WaitForSeconds(2);
        LoadGreenState();
        yield return new WaitForSeconds(greenTime);
        LoadGreenYellowState();
        yield return new WaitForSeconds(2);
        LoadRedState();
    }

    private enum MaterialColor 
    { 
        Yellow = 0,
        Red = 1,
        Green = 2
    }

    private void LoadRedState()
    {
        LightsRenderer.materials[(int)MaterialColor.Red].EnableEmission();
        LightsRenderer.materials[(int)MaterialColor.Yellow].DisableEmission();
        LightsRenderer.materials[(int)MaterialColor.Green].DisableEmission();

        CurrentState = TrafficLightState.Red;
    }

    private void LoadRedYellowState()
    {
        LightsRenderer.materials[(int)MaterialColor.Yellow].EnableEmission();

        CurrentState = TrafficLightState.RedYellow;
    }

    private void LoadGreenState()
    {
        LightsRenderer.materials[(int)MaterialColor.Red].DisableEmission();
        LightsRenderer.materials[(int)MaterialColor.Yellow].DisableEmission();
        LightsRenderer.materials[(int)MaterialColor.Green].EnableEmission();

        CurrentState = TrafficLightState.Green;
    }

    private void LoadGreenYellowState()
    {
        LightsRenderer.materials[(int)MaterialColor.Yellow].EnableEmission();

        CurrentState = TrafficLightState.GreenYellow;
    }

    private void LoadYellowBlinkingState()
    {
        if (yellowLightEnabled)
            LightsRenderer.materials[(int)MaterialColor.Yellow].DisableEmission();
        else
            LightsRenderer.materials[(int)MaterialColor.Yellow].EnableEmission();
        
        yellowLightEnabled = !yellowLightEnabled;

        CurrentState = TrafficLightState.YellowBlinking;
    }

    public enum TrafficLightState
    {
        Red = 0,
        RedYellow = 1,
        Green = 2,
        GreenYellow = 3,
        YellowBlinking = 4
    }
}

public static partial class ExtensionMethods
{
    public static Material DisableEmission(this Material material)
    {
        material.DisableKeyword("_EMISSION");
        material.globalIlluminationFlags = MaterialGlobalIlluminationFlags.EmissiveIsBlack;
        return material;
    }

    public static Material EnableEmission(this Material material)
    {
        material.EnableKeyword("_EMISSION");
        material.globalIlluminationFlags = MaterialGlobalIlluminationFlags.RealtimeEmissive;
        //material.SetColor("_EmissionColor", Color.red);
        return material;
    }
}